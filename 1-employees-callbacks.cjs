

const fs = require("fs");
const path = require("path");
const dataPath = path.join(__dirname, "data.json");
const problem1Path = path.join(__dirname, "problem1.json");
const problem2path = path.join(__dirname, "problem2.json");
const problem3path = path.join(__dirname, "problem3.json");
const problem4path = path.join(__dirname, "problem4.json");
const problem5path = path.join(__dirname, "problem5.json");
const problem6path = path.join(__dirname, "problem6.json");
const problem7path = path.join(__dirname, "problem7.json");



function employeeCallBacks() {

  fs.readFile(dataPath, "utf8", (err, data) => {
    if (err) {
      console.error(err);

    } else {
      data = JSON.parse(data);
      const keys = Object.values(data);

      const retrivedDataOfIds = keys.reduce((newObj, employee) => {
        const filteredData = employee.filter((item) => {
          return item.id == 2 || item.id == 13 || item.id == 23;

        });

        newObj = [...filteredData];

        return newObj;
      }, []);

      const content = JSON.stringify(retrivedDataOfIds, null, 2);

      fs.writeFile(problem1Path, content, (err) => {
        if (err) {
          console.error(err);

        } else {
            console.log("problem2 output saved successfully");
          //   console.log(keys);

          const arrayOfEmployessDetails = [...keys[0]];
          //   console.log(arrayOfEmployessDetails);

          const groupedData = arrayOfEmployessDetails.reduce(
            (newObj, element) => {
              if (element.company in newObj) {
                newObj[element.company].push(element);

              } else {
                newObj[element.company] = [];
                newObj[element.company].push(element);

              }
              return newObj;

            },
            {}
          );
        //   console.log(groupedData);
          const problem2Content = JSON.stringify(groupedData, null, 2);

          fs.writeFile(problem2path, problem2Content, (err) => {
            if (err) {
              console.error(err);

            } else {
              console.log("problem2 output saved successfully");

              const powerpuffBrigadedata = arrayOfEmployessDetails.filter(
                (element) => {
                  return element.company === "Powerpuff Brigade";
                }
              );
              const problem3Content = JSON.stringify(
                powerpuffBrigadedata,
                null,
                2
              );
            //   console.log(powerpuffBrigadedata);

              fs.writeFile(problem3path, problem3Content, (err) => {
                if (err) {
                  console.error(err);

                } else {
                  console.log("problem3 output saved successfully");

                  const removedID2Details = arrayOfEmployessDetails.filter(
                    (element) => {
                      return element.id !== 2;
                    }
                  );
                  const problem4Content = JSON.stringify(
                    removedID2Details,
                    null,
                    2
                  );

                  fs.writeFile(problem4path, problem4Content, (err) => {
                    if (err) {
                      console.error(err);

                    } else {
                      console.log("problem4 output saved successfully");

                      const array = [...arrayOfEmployessDetails];
                      const sortedArray = array
                        .sort((element1, element2) => {
                          return element1.id - element2.id;
                        })
                        .sort();
                      console.log(sortedArray);
                      const problem5Content = JSON.stringify(
                        sortedArray,
                        null,
                        2
                      );

                      fs.writeFile(problem5path, problem5Content, (err) => {
                        if (err) {
                          console.error(err);
                        } else {
                          console.log("problem5 output saved successfully");
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

employeeCallBacks();
